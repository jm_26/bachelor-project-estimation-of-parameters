#include "AdjointGrayScott.h"
#include "Algorithms.h"

#include <iostream>
#include <fstream> 

#define print(x) std::cout << x << std::endl

AdjointGrayScott::AdjointGrayScott(const u32 time_steps, 
                                   vec& parameters_learning)
    : time_steps(time_steps),
      parameters_learning(parameters_learning),
      current_loss(1e10)
{}

void AdjointGrayScott::Optimize(GradientMethod method) 
{

    while (current_loss > 1e-9)
    {
        SetInitialCondition(u_data, v_data, parameters_data);

        SolveForward(u_data, v_data, parameters_data);

        SetInitialCondition(u, v, parameters_learning);

        if (method == GradientMethod::ADJOINT)
        {
            SolveForward(u, v, parameters_learning);

            SetInitialCondition(lambda1, mu1, parameters_learning);

            SolveAdjoint();

            WriteSolution(lambda1, mu1, "adj.txt");

            vec grad = CalculateGrad();
            // grad[0] = Algorithms::SignedLog10(grad[0]);
            // grad[1] = Algorithms::SignedLog10(grad[1]);

            Algorithms::NormalizeVector(grad);

            print(grad[0]);
            print(grad[1]);

            const double step = current_loss < 1.0 ? 5e-6 : 5e-5;

            Algorithms::RMSprop(parameters_learning,
                                grad,
                                step,
                                0.99995);
        }

        else if (method == GradientMethod::FDM)
        {
            vec grad = CalculateGradientFDM(1e-8);

            Algorithms::NormalizeVector(grad);

            print(grad[0]);
            print(grad[1]);

            const double step = current_loss < 0.001 ? 1e-6 : 5e-5;

            Algorithms::RMSprop(parameters_learning,
                                grad,
                                step,
                                0.99995);
        }

        current_loss = GetL2Loss();

        std::cout << current_loss << " " 
                  << parameters_learning[0] << " " 
                  << parameters_learning[1] << "\n";
    } 
}

void AdjointGrayScott::SetInitialCondition(std::vector<vec>& v1, 
                                           std::vector<vec>& v2,
                                           vec parameters)
{
    // Initialize vectors

    v1.resize(time_steps, vec(nodes, 0.0));
    v2.resize(time_steps, vec(nodes, 0.0));

    // Set initial conditions for forward equations

    if (v1 == u || v1 == u_data)
    {
        // Set first chemical substance to 1.0

        for (u32 i = 0; i < nodes; ++i)
        {
            v1[0][i] = 1.0;
        }

        // Add second substance to the middle area

        const u32 width  = x_nodes / 10;
        const u32 height = y_nodes / 10;

        const u32 x_left  = x_nodes / 2 - width;
        const u32 x_right = x_nodes / 2 + width;
        const u32 y_down  = y_nodes / 2 - height;
        const u32 y_top   = y_nodes / 2 + height;

        for (u32 i = x_nodes * y_down; i < x_nodes * y_top; i++)
        {
            if (i % x_nodes > x_left && i % x_nodes < x_right)
            {
                v1[0][i] = 0.5;
                v2[0][i] = 0.25;

                // v1[0][i] += Algorithms::AddPerturbation(v1[0][i], -0.05, +0.05);
                // v2[0][i] += Algorithms::AddPerturbation(v2[0][i], -0.05, +0.05);
            }
        }

        // Adams-Bashforth
        const double f = parameters[0];
        const double k = parameters[1];

        for (u32 i = x_nodes; i < nodes - x_nodes; i++)
        {
            if (i % x_nodes != 0 && i % x_nodes != x_nodes - 1)
            {            
                v1[1][i] = v1[0][i] + dt *
                    (
                        D_u * Algorithms::Laplace2D(v1[0], i, x_nodes, dx)
                        - v1[0][i] * v2[0][i] * v2[0][i]
                        + f * (1 - v1[0][i])
                    );
                v2[1][i] = v2[0][i] + dt *
                    (
                        D_v * Algorithms::Laplace2D(v2[0], i, x_nodes, dx)
                        + v1[0][i] * v2[0][i] * v2[0][i]
                        - (f + k) * v2[0][i]
                    );
            }
        }
    }

    // Adjoint equation 

    if (v1 == lambda1)
    {
        const double f = parameters[0];
        const double k = parameters[1];

        for (u32 i = x_nodes; i < nodes - x_nodes; i++)
        {
            if (i % x_nodes != 0 && i % x_nodes != x_nodes - 1)
            {
                v1[time_steps - 2][i] = v1[time_steps - 1][i] + dt *
                (
                    u[0][i] - u_data[0][i]
                    - D_u * Algorithms::Laplace2D(v1[time_steps - 1], i, x_nodes, dx)
                    + v1[time_steps - 1][i] * (v[0][i] * v[0][i] + f)
                    - v2[time_steps - 1][i] * v[0][i] * v[0][i]
                );

                v2[time_steps - 2][i] = v2[time_steps - 1][i] + dt *
                (
                    v[0][i] - v_data[0][i]
                    - D_v * Algorithms::Laplace2D(v2[time_steps - 1], i, x_nodes, dx)
                    - 2 * v2[time_steps - 1][i] * u[0][i] * v[0][i]
                    + v2[time_steps - 1][i] * (f + k)
                    + 2 * v1[time_steps - 1][i] * u[0][i] * v[0][i]
                );
            }
        }
    }
}                   

void AdjointGrayScott::SolveForward(std::vector<vec>& u,
                                    std::vector<vec>& v,
                                    vec parameters)
{
    const double f = parameters[0];
    const double k = parameters[1];

    for (u32 t = 1; t < time_steps - 1; t++)
    {
        // Euler 

        // Skip top and bottom side

        // for (u32 i = x_nodes; i < nodes - x_nodes; i++)
        // {
        //     if (i % x_nodes != 0 && i % x_nodes != x_nodes - 1)
        //     {            
        //         // Skip left and right side

        //         u[t + 1][i] = u[t][i] + dt *
        //             (
        //                 D_u * Algorithms::Laplace2D(u[t], i, x_nodes, dx)
        //                 - u[t][i] * v[t][i] * v[t][i]
        //                 + f * (1 - u[t][i])
        //             );

        //         v[t + 1][i] = v[t][i] + dt *
        //             (
        //                 D_v * Algorithms::Laplace2D(v[t], i, x_nodes, dx)
        //                 + u[t][i] * v[t][i] * v[t][i]
        //                 - (f + k) * v[t][i]
        //             );
        //     }
        // }

        // Adams-Bashforth

        for (u32 i = x_nodes; i < nodes - x_nodes; i++)
        {
            if (i % x_nodes != 0 && i % x_nodes != x_nodes - 1)
            {            
                // Skip left and right side

                u[t + 1][i] = u[t][i] + dt *
                    (1.5 * (
                        D_u * Algorithms::Laplace2D(u[t], i, x_nodes, dx)
                        - u[t][i] * v[t][i] * v[t][i]
                        + f * (1 - u[t][i]))
                     -0.5 * (
                        D_u * Algorithms::Laplace2D(u[t-1], i, x_nodes, dx)
                        - u[t-1][i] * v[t-1][i] * v[t-1][i]
                        + f * (1 - u[t-1][i]))
                    );

                v[t + 1][i] = v[t][i] + dt *
                    (1.5 * (
                        D_v * Algorithms::Laplace2D(v[t], i, x_nodes, dx)
                        + u[t][i] * v[t][i] * v[t][i]
                        - (f + k) * v[t][i])
                     -0.5 * (
                        D_v * Algorithms::Laplace2D(v[t-1], i, x_nodes, dx)
                        + u[t-1][i] * v[t-1][i] * v[t-1][i]
                        - (f + k) * v[t-1][i])
                    );
            }
        }
    }
}

void AdjointGrayScott::SolveAdjoint()
{
    const double f = parameters_learning[0];
    const double k = parameters_learning[1];

    for (u32 t = 1; t < time_steps - 1; t++)
    {
        // Euler 

        // for (u32 i = x_nodes; i < nodes - x_nodes; i++)
        // {
        //     if (i % x_nodes != 0 && i % x_nodes != x_nodes - 1)
        //     {
        //         lambda1[time_steps - t - 2][i] = lambda1[time_steps - t - 1][i] + dt *
        //         (
        //             u[t][i] - u_data[t][i]
        //             - D_u * Algorithms::Laplace2D(lambda1[time_steps - t - 1], i, x_nodes, dx)
        //             + lambda1[time_steps - t - 1][i] * (v[t][i] * v[t][i] + f)
        //             - mu1[time_steps - t - 1][i] * v[t][i] * v[t][i]
        //         );

        //         mu1[time_steps - t - 2][i] = mu1[time_steps - t - 1][i] + dt *
        //         (
        //             v[t][i] - v_data[t][i]
        //             - D_v * Algorithms::Laplace2D(mu1[time_steps - t - 1], i, x_nodes, dx)
        //             - 2 * mu1[time_steps - t - 1][i] * u[t][i] * v[t][i]
        //             + mu1[time_steps - t - 1][i] * (f + k)
        //             + 2 * lambda1[time_steps - t - 1][i] * u[t][i] * v[t][i]
        //         );
        //     }
        // }

        // Adams-bashforth
        for (u32 i = x_nodes; i < nodes - x_nodes; i++)
        {
            if (i % x_nodes != 0 && i % x_nodes != x_nodes - 1)
            {
                lambda1[time_steps - t - 2][i] = lambda1[time_steps - t - 1][i] + dt *
                (1.5 * (
                    u[t][i] - u_data[t][i]
                    - D_u * Algorithms::Laplace2D(lambda1[time_steps - t - 1], i, x_nodes, dx)
                    + lambda1[time_steps - t - 1][i] * (v[t][i] * v[t][i] + f)
                    - mu1[time_steps - t - 1][i] * v[t][i] * v[t][i])
                 -0.5 * (
                    u[t-1][i] - u_data[t-1][i]
                    - D_u * Algorithms::Laplace2D(lambda1[time_steps - t], i, x_nodes, dx)
                    + lambda1[time_steps - t][i] * (v[t-1][i] * v[t-1][i] + f)
                    - mu1[time_steps - t][i] * v[t-1][i] * v[t-1][i])
                );

                mu1[time_steps - t - 2][i] = mu1[time_steps - t - 1][i] + dt *
                (1.5 * (
                    v[t][i] - v_data[t][i]
                    - D_v * Algorithms::Laplace2D(mu1[time_steps - t - 1], i, x_nodes, dx)
                    - 2 * mu1[time_steps - t - 1][i] * u[t][i] * v[t][i]
                    + mu1[time_steps - t - 1][i] * (f + k)
                    + 2 * lambda1[time_steps - t - 1][i] * u[t][i] * v[t][i])
                 -0.5 * (
                    v[t-1][i] - v_data[t-1][i]
                    - D_v * Algorithms::Laplace2D(mu1[time_steps - t], i, x_nodes, dx)
                    - 2 * mu1[time_steps - t][i] * u[t-1][i] * v[t-1][i]
                    + mu1[time_steps - t][i] * (f + k)
                    + 2 * lambda1[time_steps - t][i] * u[t-1][i] * v[t-1][i])
                );
            }
        }
    }
}

vec AdjointGrayScott::CalculateGrad()
{
    vec grad(parameters_count, 0.0);

    for (u32 t = 0; t < time_steps; t++)
    {
        // Skip the last row

        for (u32 i = 0; i < x_nodes * (x_nodes - 1); i++)
        {
            // Skip the last column

            if ((i + 1) % x_nodes != 0)
            {
                const double u_avg = Algorithms::TrapezoidalAverage2D(u[t], i, x_nodes);
                const double v_avg = Algorithms::TrapezoidalAverage2D(v[t], i, x_nodes);

                const double lambda1_avg = Algorithms::TrapezoidalAverage2D(lambda1[t], i, x_nodes);
                const double mu1_avg = Algorithms::TrapezoidalAverage2D(mu1[t], i, x_nodes);

                grad[0] += (lambda1_avg * (1 - u_avg) + mu1_avg * v_avg) * dx * dx;
                grad[1] += (mu1_avg * v_avg) * dx * dx;
            }
        }
    }

    return grad;
}

vec AdjointGrayScott::CalculateGradientFDM(const double epsilon)
{
    vec grad(parameters_count, 0.0);

    SolveForward(u, v, parameters_learning);

    current_loss = GetL2Loss();

    vec perturbated_parameters = parameters_learning;

    for (u32 i = 0; i < parameters_count; i++)
    {
        perturbated_parameters[i] += epsilon;
        if (i > 0) perturbated_parameters[i - 1] -= epsilon;

        SolveForward(u, v, perturbated_parameters);

        double perturbated_loss = GetL2Loss();

        grad[i] = (perturbated_loss - current_loss) / epsilon;
    }

     return grad;
}

double AdjointGrayScott::GetL2Loss()
{
    double u_loss = 0;
    double v_loss = 0;

    for (u32 t = 0; t < time_steps; t++)
    {
        // Skip last row

        for (u32 i = 0; i < x_nodes * (x_nodes - 1); i++)
        {
            // Skip last column

            if ((i + 1) % x_nodes != 0)
            {
                const double u_avg   = Algorithms::TrapezoidalAverage2D(u[t],      i, x_nodes);
                const double v_avg   = Algorithms::TrapezoidalAverage2D(v[t],      i, x_nodes);
                const double u_d_avg = Algorithms::TrapezoidalAverage2D(u_data[t], i, x_nodes);
                const double v_d_avg = Algorithms::TrapezoidalAverage2D(v_data[t], i, x_nodes);

                u_loss += (u_avg - u_d_avg) * (u_avg - u_d_avg) * dx * dx;
                v_loss += (v_avg - v_d_avg) * (v_avg - v_d_avg) * dx * dx;
            }
        }
    }

    return u_loss + v_loss;
}

void AdjointGrayScott::WriteSolution(const std::vector<vec>&u,
                                     const std::vector<vec>&v,
                                     const char* file_name)
{
    std::ofstream file;

    file.open(file_name, std::ios::out);

    for (u32 i = 0; i < nodes; i++)
    {
        double x = (i % x_nodes) * dx;
        double y = static_cast<int>((i / y_nodes)) * dy;

        file << std::fixed << x << "\t" << y << "\t"
                           << u[1][i] << "\t" << v[1][i] << "\n";

        // file << std::fixed << x << "\t" << y << "\t"
        //                    << u[time_steps - 50][i] << "\t" << v[time_steps - 50][i] << "\n";
    }

    file.close();
}
