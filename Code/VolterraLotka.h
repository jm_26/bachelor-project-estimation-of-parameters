#pragma once 

#include "BaseOptimizerODE.h"

#include "MyTypedefs.h"

class VolterraLotka : public BaseOptimizerODE
{

public:

    VolterraLotka(const vec& known_parameters,
                  const vec& learning_parameters,
                  const double final_time,
                  const double tau,
                  const vec& forward_initial_condition);

    void Optimize(GradientMethod method);

private:

    const u32 parameters_count = 4;

    vec x, y;

    vec x_data, y_data;

    vec lambda1, lambda2;

    vec theta1, theta2;

    void SetInitialConditions();

    void SolveForward(vec& v1, vec& v2, const vec& params);

    void SolveAdjoint();

    vec ComputeGradient();

    vec ComputeGradientFDM(const double epsilon);

    double GetL2Loss() const;

    void ComputeGradientTheta(const double learning_rate, const double gamma);

    void WriteData(const char* file_name, const vec& data) const;
};
