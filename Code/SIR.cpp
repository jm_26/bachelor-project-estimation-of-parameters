#include "SIR.h"

#include "Algorithms.h"

#include <fstream>
#include <iostream>

#define print(x) std::cout << x << "\n"

SIR::SIR(const vec& known_parameters,
         const vec& learning_parameters,
         const double final_time,
         const double tau, 
         const vec& forward_initial_condition)
    : BaseOptimizerODE(known_parameters,
                       learning_parameters,
                       final_time,
                       tau,
                       forward_initial_condition),
      N(forward_initial_condition[0] + forward_initial_condition[1] + forward_initial_condition[2]),
      S(time_steps, 0.0),
      I(time_steps, 0.0),
      R(time_steps, 0.0),
      S_data(time_steps, 0.0),
      I_data(time_steps, 0.0),
      R_data(time_steps, 0.0),
      lambda1(time_steps, 0.0),
      lambda2(time_steps, 0.0),
      lambda3(time_steps, 0.0)
{}

void SIR::Optimize(GradientMethod method)
{
    SetInitialConditions();

    SolveForward(S_data, I_data, R_data, known_parameters);

    WriteData("data/sir/s1.txt", S_data);
    WriteData("data/sir/i1.txt", I_data);
    WriteData("data/sir/r1.txt", R_data);

    for (u32 i = 0; ; ++i)
    {
        if (method == GradientMethod::ADJOINT)
        {
            SolveForward(S, I, R, learning_parameters);

            SolveAdjoint();

            vec gradient = ComputeGradient();

            Algorithms::RMSprop(learning_parameters, 
                                gradient,
                                1e-5,
                                0.99);
        }

        else if (method == GradientMethod::FDM)
        {
            vec gradient = ComputeGradientFDM(1e-8);

            Algorithms::NormalizeVector(gradient);

            Algorithms::RMSprop(learning_parameters, 
                                gradient,
                                1e-4, 
                                0.99);
        }

        current_loss = GetL2Loss();

        if (i % 10 == 0)
        {
            std::cout << "L2 Loss: " << i << " " 
                << current_loss           << " "
                << learning_parameters[0] << " "
                << learning_parameters[1] << "\n";
        }

        if (current_loss < 1e-10)
        {
            std::cout << "Optimization finished succesfully.\n";
            break;
        }

        if (current_loss != current_loss)
        {
            std::cout << "Optimization failed.\n";
            break;
        }
    }
}

void SIR::SetInitialConditions()
{
    S[0] = forward_initial_condition[0];
    I[0] = forward_initial_condition[1];
    R[0] = forward_initial_condition[2];

    S[1] = forward_initial_condition[0];
    I[1] = forward_initial_condition[1];
    R[1] = forward_initial_condition[2];


    S_data[0] = forward_initial_condition[0];
    I_data[0] = forward_initial_condition[1];
    R_data[0] = forward_initial_condition[2];

    S_data[1] = forward_initial_condition[0];
    I_data[1] = forward_initial_condition[1];
    R_data[1] = forward_initial_condition[2];
}

void SIR::SolveForward(vec& v1, vec& v2, vec& v3, const vec& params)
{
    const double b = params[0];    // beta 
    const double g = params[1];    // gamma

    for (u32 t = 2; t < time_steps; ++t)
    {
        // Euler 

        // v1[t] = v1[t - 1] + tau * (-b * v2[t - 1] * v1[t - 1] / N);

        // v2[t] = v2[t - 1] + tau * (b * v2[t - 1] * v1[t - 1] / N - g * v2[t - 1]);

        // v3[t] = v3[t - 1] + tau * (g * v2[t - 1]);

        // Two steps Adams-Bashforth

        v1[t] = v1[t - 1] + tau * 
                (1.5 * (-b * v2[t - 1] * v1[t - 1] / N) - 
                 0.5 * (-b * v2[t - 2] * v1[t - 2] / N));

        v2[t] = v2[t - 1] + tau * 
                (1.5 * (b * v2[t - 1] * v1[t - 1] / N - g * v2[t - 1]) - 
                 0.5 * (b * v2[t - 2] * v1[t - 2] / N - g * v2[t - 2]));

        v3[t] = v3[t - 1] + tau * 
                (1.5 * (g * v2[t - 1]) - 
                 0.5 * (g * v2[t - 2]));
    }
}

void SIR::SolveAdjoint()
{
    const double b = learning_parameters[0]; 
    const double g = learning_parameters[1]; 

    for (u32 t = 2; t < time_steps; ++t)
    {
        // Euler 

        // lambda1[time_steps - 1 - t] = lambda1[time_steps - t] + tau * 
        // (
        //     2 * (S[t] - S_data[t])
        //     + lambda1[time_steps - t] * b * I[t] / N 
        //     - lambda2[time_steps - t] * b * I[t] / N
        // );

        // lambda2[time_steps - 1 - t] = lambda2[time_steps - t] + tau * 
        // (
        //     2 * (I[t] - I_data[t])
        //     + lambda1[time_steps - t] *  b * S[t] / N 
        //     - lambda2[time_steps - t] * (b * S[t] / N - g)
        //     - lambda3[time_steps - t] * g                  
        // );

        // lambda3[time_steps - 1 - t] = lambda3[time_steps - t] + tau * 
        // (
        //     2 * (R[t] - R_data[t])
        // );

        // Two steps Adams-basforth

        lambda1[time_steps - 1 - t] = lambda1[time_steps - t] + tau * (
        1.5 * 
        (
            2 * (S[t] - S_data[t])
            + lambda1[time_steps - t] * b * I[t] / N 
            - lambda2[time_steps - t] * b * I[t] / N
        ) - 
        0.5 * 
        (
            2 * (S[t - 1] - S_data[t - 1])
            + lambda1[time_steps + 1 - t] * b * I[t - 1] / N 
            - lambda2[time_steps + 1 - t] * b * I[t - 1] / N
        ));

        lambda2[time_steps - 1 - t] = lambda2[time_steps - t] + tau * (
        1.5 * 
        (
            2 * (I[t] - I_data[t])
            + lambda1[time_steps - t] *  b * S[t] / N 
            - lambda2[time_steps - t] * (b * S[t] / N - g)
            - lambda3[time_steps - t] * g                  
        ) - 
        0.5 * 
        (
            2 * (I[t - 1] - I_data[t - 1])
            + lambda1[time_steps + 1 - t] *  b * S[t - 1] / N 
            - lambda2[time_steps + 1 - t] * (b * S[t - 1] / N - g)
            - lambda3[time_steps + 1 - t] * g
        ));

        lambda3[time_steps - 1 - t] = lambda3[time_steps - t] + tau * 
        (
            1.5 * (2 * (R[t] - R_data[t])) 
          - 0.5 * (2 * (R[t - 1] - R_data[t - 1]))
        );
    }
}

vec SIR::ComputeGradient()
{
    vec gradient(parameters_count, 0.0);

    for (u32 t = 0; t < time_steps; ++t)
    {
        lambda1[t] *= -1;
        lambda2[t] *= -1;
        lambda3[t] *= -1;


        gradient[0] += (lambda1[t] - lambda2[t]) * I[t] * S[t] / N * tau;
        gradient[1] += (lambda2[t] - lambda3[t]) * I[t] * tau;
    }

    return gradient;
}

vec SIR::ComputeGradientFDM(const double epsilon)
{
    vec gradient(parameters_count, 0.0);

    SolveForward(S, I, R, learning_parameters);

    current_loss = GetL2Loss();

    vec perturbated_parameters = learning_parameters;

    for (u32 i = 0; i < parameters_count; ++i)
    {
        perturbated_parameters[i] += epsilon;
        if (i > 0) perturbated_parameters[i - 1] -= epsilon;

        SolveForward(S, I, R, perturbated_parameters);

        const double perturbated_loss = GetL2Loss();
        
        gradient[i] = (perturbated_loss - current_loss) / epsilon;
    }

    return gradient;
}

double SIR::GetL2Loss() const
{
    double loss = 0;

    for (u32 t = 0; t < time_steps; ++t)
    {
        const double S_loss = (S[t] - S_data[t]) * (S[t] - S_data[t]);
        const double I_loss = (I[t] - I_data[t]) * (I[t] - I_data[t]);
        const double R_loss = (R[t] - R_data[t]) * (R[t] - R_data[t]);

        loss += S_loss + I_loss + R_loss;
    }

    return loss * tau;
}

void SIR::WriteData(const char* file_name, const vec& data) const
{
    std::ofstream file;
    file.open(file_name, std::ios::out);

    for (u32 i = 0; i < data.size(); ++i)
    {
        file << std::fixed << i * tau << "\t" << data[i] << std::endl;
    }

    file.close();
}