#include "Lorenz.h"

#include "Timer.h"
#include "MyTypedefs.h"

int main()
{
    Timer timer;

    // const vec known_parameters = {10.0, 28.0, 2.66};

    const vec known_parameters = {10.0, 28.0, 2.66};

    vec learning_parameters = {10.0, 30, 2.66}; 

    const double final_time = 5;

    const double tau = 1e-3;

    const vec forward_initial_condition = {1.0, 1.0, 1.0};

    Lorenz Optimizer(known_parameters,
                     learning_parameters,
                     final_time,
                     tau,
                     forward_initial_condition);

    Optimizer.Optimize(GradientMethod::FDM);
}