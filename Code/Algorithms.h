#pragma once

#include "MyTypedefs.h"

class Algorithms
{
public:
    static double Laplace2D(const vec& v,
                            const u32 i,
                            const u32 x_nodes,
                            const double dx);

    static double TrapezoidalAverage2D(const vec& v,
                                       const u32 i,
                                       const u32 x_nodes);

    static double VectorNorm(vec& v);

    static void NormalizeVector(vec& v);

    static void MeanNormalization(vec& v);

    static void MinMaxNormalization(vec& v);

    static void ZScoreNormalization(vec& v);

    static double SignedLog10(const double x);

    static double SignedLogBase(const double x, const double base);

    static vec RandomVector(const u32 size,
                            const double min,
                            const double max);

    static void PerturbateVector(vec& v,	
                                 const double lower_bound,
                                 const double upper_bound);

    static double AddPerturbation(const double num, const double min, const double max);

    static void GradientDescent(vec& parameters,
                                const vec& grad,
                                const double step);

    static void MomentumGD(vec& learning_parameters,
                           vec& grad,
                           const double learning_rate,
                           const double momentum_rate);

    static void RMSprop(vec& learning_parameters,
                        vec& grad,
                        const double learning_rate,
                        const double rmsp_rate);

    static void ADAM(vec& params,
                     vec& grad,
                     vec& m,
                     vec& v,
                     const double step);
};