#pragma once

#include "MyTypedefs.h"

enum class GradientMethod
{
    ADJOINT, 
    FDM
};

struct GSProblem
{
    const double D_u = 2e-5;
    const double D_v = 1e-5;

    const double x_size = 1.0;
    const double y_size = 1.0;

    const u32 x_nodes = 51;
    const u32 y_nodes = 51;

    const u32 nodes = x_nodes * y_nodes;

    const double dx = x_size / (x_nodes - 1.0);
    const double dy = y_size / (y_nodes - 1.0);

    const double dt = 0.1;
};

class AdjointGrayScott : public GSProblem
{

private:

    const u32 parameters_count = 2;

    const vec parameters_data = {0.029, 0.057};

    u32 time_steps;

public:

    AdjointGrayScott(const u32 time_steps, 
                     vec& parameters_learning);

    void Optimize(GradientMethod method);

private:

    vec parameters_learning;

    std::vector<vec> u, v;

    std::vector<vec> u_data, v_data;

    std::vector<vec> lambda1, mu1;

    double current_loss;

    void SetInitialCondition(std::vector<vec>& v1,
                             std::vector<vec>& v2,
                             vec parameters);

    void SolveForward(std::vector<vec>& u,
                      std::vector<vec>& v,
                      vec parameters);

    void SolveAdjoint();

    vec CalculateGrad();

    vec CalculateGradientFDM(const double epsilon);

    double GetL2Loss();

    void WriteSolution(const std::vector<vec>&u,
                       const std::vector<vec>&v,
                       const char* file_name);
};