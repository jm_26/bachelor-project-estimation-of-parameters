#include "AdjointGrayScott.h"

#include "Timer.h"
#include "MyTypedefs.h"

int main()
{
    Timer timer;

    const u32 time_steps = 400;

    vec parameters_learning = {0.03, 0.03};

    AdjointGrayScott Optimizer(time_steps, parameters_learning);

    Optimizer.Optimize(GradientMethod::FDM);   
}