#pragma once

#include <cstdint>
#include <vector>

typedef uint32_t u32;

typedef std::vector<double> vec;

typedef std::vector<std::vector<double>> vec2D;