#include "SIR.h"

#include "Timer.h"
#include "MyTypedefs.h"

int main()
{
    Timer timer;

    // const vec known_parameters = {0.0561215, 0.045533};

    const vec known_parameters = {1.2, 0.4};

    const vec learning_parameters = {0.5, 0.5};

    const double final_time = 5;

    const double tau = 1e-2;

    const vec forward_initial_condition = {990, 10, 0};

    SIR Optimizer(known_parameters, 
                  learning_parameters, 
                  final_time, 
                  tau,
                  forward_initial_condition);

    Optimizer.Optimize(GradientMethod::ADJOINT);
}