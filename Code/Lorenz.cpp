#include "Lorenz.h"

#include "Algorithms.h"

#include <iostream>
#include <fstream>

#define print(x) std::cout << x << std::endl

Lorenz::Lorenz(const vec& known_parameters,
               const vec& learning_parameters,
               const double final_time,
               const double tau, 
               const vec& forward_initial_condition)
    : BaseOptimizerODE(known_parameters,
                       learning_parameters,
                       final_time,
                       tau,
                       forward_initial_condition),
      x(time_steps, 0.0),
      y(time_steps, 0.0),
      z(time_steps, 0.0),
      x_data(time_steps, 0.0),
      y_data(time_steps, 0.0),
      z_data(time_steps, 0.0),
      lambda1(time_steps, 0.0),
      lambda2(time_steps, 0.0),
      lambda3(time_steps, 0.0)
    {}

void Lorenz::Optimize(GradientMethod method)
{
    SetInitialConditions();

    SolveForward(x_data, y_data, z_data, known_parameters);

    WriteData("data/lorenz/x.txt", x_data);
    WriteData("data/lorenz/y.txt", y_data);
    WriteData("data/lorenz/z.txt", z_data);

    for (u32 i = 0; ; ++i)
    {
        if (method == GradientMethod::ADJOINT)
        {
            SolveForward(x, y, z, learning_parameters); 

            SolveAdjoint();

            vec gradient = ComputeGradient();

            Algorithms::ZScoreNormalization(gradient);

            // std::cout << "Gradient 0 " << gradient[0] << std::endl;
            // std::cout << "Gradient 1 " << gradient[1] << std::endl;
            // std::cout << "Gradient 2 " << gradient[2] << std::endl;

            Algorithms::RMSprop(learning_parameters,
                                gradient,
                                1e-4, 
                                0.99999);
            // Algorithms::GradientDescent(learning_parameters, gradient, 1e-4);
        }

        else if (method == GradientMethod::FDM)
        {
            vec gradient = ComputeGradientFDM(1e-8);

            Algorithms::NormalizeVector(gradient);

            // std::cout << "Gradient 0 " << gradient[0] << std::endl;
            // std::cout << "Gradient 1 " << gradient[1] << std::endl;
            // std::cout << "Gradient 2 " << gradient[2] << std::endl;

            Algorithms::RMSprop(learning_parameters,
                                gradient,
                                1e-4, 
                                0.99);
        }

        current_loss = GetL2Loss();

        if (i % 5 == 0)
        {
            std::cout << "L2 Loss: " << i << " " 
                << current_loss           << " "
                << learning_parameters[0] << " "
                << learning_parameters[1] << " "
                << learning_parameters[2] << "\n";
        }

        if (current_loss < 0.00001)
        {
            std::cout << "Optimization finished succesfully.\n";
            break;
        }

        if (current_loss != current_loss)
        {
            std::cout << "Optimization failed.\n";
            break;
        }
    }
}

void Lorenz::SetInitialConditions()
{
    x[0] = forward_initial_condition[0];
    y[0] = forward_initial_condition[1];
    z[0] = forward_initial_condition[2];

    x[1] = x[0] + tau * (learning_parameters[0] * (y[0] - x[0]));
    y[1] = y[0] + tau * (x[0] * (learning_parameters[1] - z[0]) - y[0]);
    z[1] = z[0] + tau * (x[0] * y[0] - learning_parameters[2] * z[0]);

    x_data[0] = forward_initial_condition[0];
    y_data[0] = forward_initial_condition[1];
    z_data[0] = forward_initial_condition[2];

    x_data[1] = x_data[0] + tau * (known_parameters[0] * (y[0] - x[0]));
    y_data[1] = y_data[0] + tau * (x[0] * (known_parameters[1] - z[0]) - y[0]);
    z_data[1] = z_data[0] + tau * (x[0] * y[0] - known_parameters[2] * z[0]);
}

void Lorenz::SolveForward(vec& v1, vec& v2, vec& v3, const vec& params)
{
    const double s = params[0];   // sigma
    const double r = params[1];   // rho
    const double b = params[2];   // beta

    for (u32 t = 2; t < time_steps; ++t)
    {
        v1[t] = v1[t - 1] + tau * 
        ( 1.5 * s * (v2[t - 1] - v1[t - 1])
         -0.5 * s * (v2[t - 2] - v1[t - 2]));

        v2[t] = v2[t - 1] + tau * 
        ( 1.5 * (v1[t - 1] * (r - v3[t - 1]) - v2[t - 1])
         -0.5 * (v1[t - 2] * (r - v3[t - 2]) - v2[t - 2]));

        v3[t] = v3[t - 1] + tau * 
        ( 1.5 * (v1[t - 1] * v2[t - 1] - b * v3[t - 1])
         -0.5 * (v1[t - 2] * v2[t - 2] - b * v3[t - 2]));
    }
}

void Lorenz::SolveAdjoint()
{
    const double s = learning_parameters[0];
    const double r = learning_parameters[1];
    const double b = learning_parameters[2];

    for (u32 t = 2; t < time_steps; ++t)
    {
        lambda1[time_steps - 1 - t] = lambda1[time_steps - t] + tau * 
        (1.5 * (
            2 * (x[t] - x_data[t])
            + lambda1[time_steps - t] * s
            - lambda2[time_steps - t] * (r - z[t])
            - lambda3[time_steps - t] * y[t])
        -0.5 * (
            2 * (x[t - 1] - x_data[t - 1])
            + lambda1[time_steps - t + 1] * s
            - lambda2[time_steps - t + 1] * (r - z[t - 1])
            - lambda3[time_steps - t + 1] * y[t - 1])
        );

        lambda2[time_steps - 1 - t] = lambda2[time_steps - t] + tau * 
        (1.5 * (
            2 * (y[t] - y_data[t]) 
            - lambda1[time_steps - t] * s
            + lambda2[time_steps - t] 
            - lambda3[time_steps - t] * x[t])
        -0.5 * (
            2 * (y[t - 1] - y_data[t - 1]) 
            - lambda1[time_steps - t + 1] * s
            + lambda2[time_steps - t + 1] 
            - lambda3[time_steps - t + 1] * x[t - 1])
        );

        lambda3[time_steps - 1 - t] = lambda3[time_steps - t] + tau * 
        (1.5 * (
            2 * (z[t] - z_data[t]) 
            + lambda2[time_steps - t] * x[t]
            + lambda3[time_steps - t] * b)
        -0.5 * (
            2 * (z[t - 1] - z_data[t - 1]) 
            + lambda2[time_steps - t + 1] * x[t - 1]
            + lambda3[time_steps - t + 1] * b)
        );
    }
}

vec Lorenz::ComputeGradient()
{
    vec gradient(parameters_count, 0.0); 

    for (u32 t = 0; t < time_steps; ++t)
    {
        gradient[0] +=  lambda1[t] * (x[t] - y[t]) * tau;
        gradient[1] += -lambda2[t] * x[t] * tau;
        gradient[2] +=  lambda3[t] * z[t] * tau;
    }

    return gradient;
}

vec Lorenz::ComputeGradientFDM(const double epsilon)
{
    vec gradient(parameters_count, 0.0); 

    SolveForward(x, y, z, learning_parameters);

    current_loss = GetL2Loss();

    vec perturbated_parameters = learning_parameters;

    for (u32 i = 0; i < parameters_count; ++i)
    {
        perturbated_parameters[i] += epsilon;
        if (i > 0) perturbated_parameters[i - 1] -= epsilon;

        SolveForward(x, y, z, perturbated_parameters); 

        const double perturbated_loss = GetL2Loss();

        gradient[i] = (perturbated_loss - current_loss) / epsilon;
    }
    
    return gradient;
}

double Lorenz::GetL2Loss() const 
{
    double loss = 0;

    for (u32 t = 0; t < time_steps; ++t)
    {
        const double x_loss = (x[t] - x_data[t]) * (x[t] - x_data[t]);
        const double y_loss = (y[t] - y_data[t]) * (y[t] - y_data[t]);
        const double z_loss = (z[t] - z_data[t]) * (z[t] - z_data[t]);

        loss += x_loss + y_loss + z_loss;
    }

    return loss * tau; 
}

void Lorenz::WriteData(const char* file_name, const vec& data) const
{
    std::ofstream file;
    file.open(file_name, std::ios::out);

    for (u32 i = 0; i < data.size(); ++i)
    {
        file << std::fixed << i * tau << "\t" << data[i] << std::endl;
    }

    file.close();
}