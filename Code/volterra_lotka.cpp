#include "VolterraLotka.h"

#include "Timer.h"
#include "MyTypedefs.h"

int main()
{
    Timer timer; 

    const vec known_parameters = {0.5471, 0.281, 0.8439, 0.0266};

    vec learning_parameters(4);
    learning_parameters[0] = 0.1;
    learning_parameters[1] = 0.1;
    learning_parameters[2] = 0.1;
    learning_parameters[3] = 0.01;

    const double final_time = 5;

    const double tau = 1e-4;

    const vec forward_initial_condition = {30, 4};

    VolterraLotka Optimizer(known_parameters,
                            learning_parameters,
                            final_time,
                            tau,
                            forward_initial_condition);

    Optimizer.Optimize(GradientMethod::ADJOINT);
}