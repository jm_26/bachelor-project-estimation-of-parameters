#pragma once 

#include "BaseOptimizerODE.h"

#include "MyTypedefs.h"

class SIR : public BaseOptimizerODE
{

public: 

    SIR(const vec& known_parameters,
        const vec& learning_parameters,
        const double final_time,
        const double tau, 
        const vec& forward_initial_condition);

    void Optimize(GradientMethod method);

private: 

    const u32 parameters_count = 2;

    const u32 N;

    vec S, I, R;

    vec S_data, I_data, R_data;

    vec lambda1, lambda2, lambda3;

    void SetInitialConditions();

    void SolveForward(vec& v1, vec& v2, vec& v3, const vec& params);

    void SolveAdjoint();

    vec ComputeGradient();

    vec ComputeGradientFDM(const double epsilon);

    double GetL2Loss() const;

    void WriteData(const char* file_name, const vec& data) const;
};
