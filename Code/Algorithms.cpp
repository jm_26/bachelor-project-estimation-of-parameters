#include "Algorithms.h"

#include <algorithm>
#include <numeric>
#include <cmath>
#include <random>
#include <chrono>

double Algorithms::Laplace2D(const vec& v,
                             const u32 i,
                             const u32 x_nodes,
                             const double dx)
{
    return (v[i - 1] + v[i + 1] + v[i - x_nodes] + v[i + x_nodes] - 4 * v[i]) / (dx * dx);
}

double Algorithms::TrapezoidalAverage2D(const vec& v,
                                        const u32 i,
                                        const u32 x_nodes)
{
    return (v[i] + v[i + 1] + v[i + x_nodes] + v[i + x_nodes + 1]) * 0.25;
}

double Algorithms::VectorNorm(vec& v)
{
    return std::sqrt(std::inner_product(v.begin(), v.end(), v.begin(), 0.0));
}

void Algorithms::NormalizeVector(vec& v)
{
    double grad_norm = VectorNorm(v);

    std::for_each(v.begin(), v.end(), [grad_norm](double& d){ d /= grad_norm; });
}

void Algorithms::MeanNormalization(vec& v)
{
    const double mean = std::accumulate(v.begin(), v.end(), 0.0) / v.size();
    
    const double max = *std::max_element(v.begin(), v.end());
    const double min = *std::min_element(v.begin(), v.end());

    for (u32 i = 0; i < v.size(); i++)
        v[i] = (v[i] - mean) / (max - min);
}

void Algorithms::MinMaxNormalization(vec& v)
{
    const double max = *std::max_element(v.begin(), v.end());
    const double min = *std::min_element(v.begin(), v.end());

    for (u32 i = 0; i < v.size(); i++)
        v[i] = (v[i] - min) / (max - min);
}

void Algorithms::ZScoreNormalization(vec &v)
{
    const double mean = std::accumulate(v.begin(), v.end(), 0.0) / v.size();

    vec diff(v.size());
    std::transform(v.begin(), v.end(), diff.begin(), [mean](double x) { return x - mean; });
    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / v.size());

    for (u32 i = 0; i < v.size(); i++)
        v[i] = (v[i] - mean) / stdev;
}

double Algorithms::SignedLog10(const double x)
{
    return std::copysign(std::log10(std::fabs(x)), x);
}

double Algorithms::SignedLogBase(const double x, const double base)
{
    if (x == 0.0) {
        return 0.0;
    } else if (x < 0.0) {
        // For negative value, return negative of log of its absolute value.
        return -std::log(std::abs(x)) / std::log(base);
    } else {
        // For positive value, return log of the value.
        return std::log(x) / std::log(base);
    }
}

vec Algorithms::RandomVector(const u32 size,
                             const double min,
                             const double max)
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine gen(seed);
    std::uniform_real_distribution<double> dis(min, max);

    vec v(size);

    for (u32 i = 0; i < size; i++)
        v[i] = dis(gen);

    return v;
}

void Algorithms::PerturbateVector(vec& v,
                                  const double lower_bound,
                                  const double upper_bound)
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);

    std::uniform_int_distribution<int> int_distribution(0, v.size() - 1);

    std::uniform_real_distribution<double> double_distribution(lower_bound, upper_bound);

    u32 position = int_distribution(generator);
    double perturbation = double_distribution(generator);

    v[position] += perturbation;
}

double Algorithms::AddPerturbation(const double num, const double min, const double max)
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

    // Create a random number generator
    std::default_random_engine generator(seed);

    // Create a uniform distribution between min and max
    std::uniform_real_distribution<double> distribution(min, max);

    // Generate a random number from the distribution
    double random_number = distribution(generator);

    // Add the random number to the input
    double output = num + random_number;

    return output;
}

void Algorithms::GradientDescent(vec& parameters,
                                 const vec& grad,
                                 const double step)
{
    for (u32 i = 0; i < parameters.size(); i++)
    {
        parameters[i] -= step * grad[i];
    }
}

void Algorithms::MomentumGD(vec& learning_parameters,
                            vec& grad,
                            const double learning_rate,
                            const double momentum_rate)
{

    static vec grad_update(learning_parameters.size(), 0.0);

    for (u32 i = 0; i < learning_parameters.size(); i++)
    {
        grad_update[i] = momentum_rate * grad_update[i] - learning_rate * grad[i];
        learning_parameters[i] += grad_update[i];
    }
}

void Algorithms::RMSprop(vec& learning_parameters,
                         vec& grad,
                         const double learning_rate,
                         const double rmsp_rate)
{
    static vec grad_update(learning_parameters.size(), 0.0);

    for (u32 i = 0; i < learning_parameters.size(); i++)
    {
        grad_update[i] = rmsp_rate * grad_update[i] + (1.0 - rmsp_rate) * grad[i] * grad[i];
        learning_parameters[i] -= learning_rate * grad[i] / sqrt(grad_update[i] + 1e-8);
    }
}

void Algorithms::ADAM(vec& params,
                      vec& grad,
                      vec& m,
                      vec& v,
                      const double step)
{
    if (m.empty() || v.empty())
    {
        m.resize(params.size(), 0.0);
        v.resize(params.size(), 0.0);
    }

    const double beta1 = 0.9;
    const double beta2 = 0.999;
    const double epsilon = 1e-8;

    static u32 t = 0;
    t++;

    for (u32 i = 0; i < params.size(); ++i)
    {
        m[i] = beta1 * m[i] + (1.0 - beta1) * grad[i];
        v[i] = beta2 * v[i] + (1.0 - beta2) * grad[i] * grad[i];

        double m_hat = m[i] / (1.0 - std::pow(beta1, t));
        double v_hat = v[i] / (1.0 - std::pow(beta2, t));

        params[i] -= step * m_hat / (std::sqrt(v_hat) + epsilon);
    }
}
