#include "VolterraLotka.h"

#include "Algorithms.h"

#include <fstream>
#include <iostream>

#define print(x) std::cout << x << "\n"

VolterraLotka::VolterraLotka(const vec& known_parameters,
                             const vec& learning_parameters,
                             const double final_time,
                             const double tau, 
                             const vec& forward_initial_condition)
    : BaseOptimizerODE(known_parameters,
                       learning_parameters,
                       final_time,
                       tau,
                       forward_initial_condition),
      x(time_steps, 0.0), 
      y(time_steps, 0.0),
      x_data(time_steps, 0.0), 
      y_data(time_steps, 0.0),
      lambda1(time_steps, 0.0), 
      lambda2(time_steps, 0.0),
      theta1(time_steps, 0.0),
      theta2(time_steps, 0.0)
{}

void VolterraLotka::Optimize(GradientMethod method)
{
    SetInitialConditions();

    SolveForward(x_data, y_data, known_parameters);

    WriteData("data/vl/x.txt", x_data);
    WriteData("data/vl/y.txt", y_data);

    for (u32 i = 0; ; ++i)
    {
        if (method == GradientMethod::ADJOINT)
        {
            SolveForward(x, y, learning_parameters);

            SolveAdjoint();

            vec gradient = ComputeGradient();

            ComputeGradientTheta(0.01, 100);

            current_loss < 100 ? Algorithms::NormalizeVector(gradient) : Algorithms::ZScoreNormalization(gradient);

            const double step = current_loss < 1.0 ? 1e-5 : 1e-4;

            Algorithms::RMSprop(learning_parameters, 
                                gradient,
                                step,
                                0.99);
        }

        else if (method == GradientMethod::FDM)
        {
            vec gradient = ComputeGradientFDM(1e-8);

            current_loss < 100 ? Algorithms::NormalizeVector(gradient) : Algorithms::MeanNormalization(gradient);

            const double step = current_loss < 1.0 ? 1e-5 : 1e-4;

            Algorithms::RMSprop(learning_parameters, 
                                gradient,
                                step, 
                                0.99);
        }

        current_loss = GetL2Loss();

        if (i % 50 == 0)
        {
            std::cout << "L2 Loss: " << i << " " 
                << current_loss           << " "
                << learning_parameters[0] << " "
                << learning_parameters[1] << " "
                << learning_parameters[2] << " "
                << learning_parameters[3] << "\n";
        }

        if (current_loss < 0.0001)
        {
            std::cout << "Optimization finished succesfully.\n";
            print(lambda1[0]);
            print(lambda2[0]);
            break;
        }

        if (current_loss != current_loss)
        {
            std::cout << "Optimization failed.\n";
            print(lambda1[0]);
            print(lambda2[0]);
            break;
        }
    }
}

void VolterraLotka::SetInitialConditions()
{
    x[0] = forward_initial_condition[0];
    y[0] = forward_initial_condition[1];

    x[1] = x[0] + tau * ( learning_parameters[0] * x[0] - learning_parameters[1] * x[0] * y[0]);
    y[1] = y[0] + tau * (-learning_parameters[2] * y[0] + learning_parameters[3] * x[0] * y[0]);

    x_data[0] = forward_initial_condition[0];
    y_data[0] = forward_initial_condition[1];

    x_data[1] = x_data[0] + tau * ( known_parameters[0] * x_data[0] - known_parameters[1] * x_data[0] * y_data[0]);
    y_data[1] = y_data[0] + tau * (-known_parameters[2] * y_data[0] + known_parameters[3] * x_data[0] * y_data[0]);
}

void VolterraLotka::SolveForward(vec& v1, vec& v2, const vec& params)
{
    const double a1 = params[0];    // alpha1 
    const double b1 = params[1];    // beta1
    const double a2 = params[2];    // alpha2
    const double b2 = params[3];    // beta2

    for (u32 t = 2; t < time_steps; ++t)
    {
        // Euler 

        // v1[t] = v1[t - 1] + tau * ( a1 * v1[t - 1] - b1 * v1[t - 1] * v2[t - 1] + theta1[t]);

        // v2[t] = v2[t - 1] + tau * (-a2 * v2[t - 1] + b2 * v1[t - 1] * v2[t - 1] + theta2[t]);

        // Two steps Adams-Bashforth

        v1[t] = v1[t - 1] + tau *
                (1.5 * ( a1 * v1[t - 1] - b1 * v1[t - 1] * v2[t - 1]) -
                 0.5 * ( a1 * v1[t - 2] - b1 * v1[t - 2] * v2[t - 2]) + 
                 theta1[t]);

        v2[t] = v2[t - 1] + tau *
                (1.5 * (-a2 * v2[t - 1] + b2 * v1[t - 1] * v2[t - 1]) -
                 0.5 * (-a2 * v2[t - 2] + b2 * v1[t - 2] * v2[t - 2]) + 
                 theta2[t]);
    }
}

void VolterraLotka::SolveAdjoint()
{
    const double a1 = learning_parameters[0];
    const double b1 = learning_parameters[1];
    const double a2 = learning_parameters[2];
    const double b2 = learning_parameters[3];

    for (u32 t = 2; t < time_steps; ++t)
    {
        // Euler 

        // lambda1[time_steps - 1 - t] = lambda1[time_steps - t] + tau * 
        // (
        //     2 * (x[t] - x_data[t])
        //     - lambda1[time_steps - t] * (a1 - b1 * y[t])
        //     - lambda2[time_steps - t] * b2 * y[t]
        // );
            
        // lambda2[time_steps - 1 - t] = lambda2[time_steps - t] + tau * 
        // (
        //     2 * (y[t] - y_data[t])
        //     + lambda1[time_steps - t] * b1 * x[t]
        //     - lambda2[time_steps - t] * (-a2 + b2 * x[t])
        // );

        // Two steps Adams-Bashforth

        lambda1[time_steps - 1 - t] = lambda1[time_steps - t] + tau *
                 (1.5 * (2 * (x[t] - x_data[t]) -
                         lambda1[time_steps - t] * (a1 - b1 * y[t]) -
                         lambda2[time_steps - t] *  b2 * y[t]) -
                  0.5 * (2 * (x[t - 1] - x_data[t - 1]) -
                         lambda1[time_steps + 1 - t] * (a1 - b1 * y[t - 1]) -
                         lambda2[time_steps + 1 - t] *  b2 * y[t - 1]));

        lambda2[time_steps - 1 - t] = lambda2[time_steps - t] + tau *
                 (1.5 * (2 * (y[t] - y_data[t]) -
                         lambda2[time_steps - t] * (-a2 + b2 * x[t]) +
                         lambda1[time_steps - t] *  b1 * x[t]) -
                  0.5 * (2 * (y[t - 1] - y_data[t - 1]) -
                         lambda2[time_steps + 1 - t] * (-a2 + b2 * x[t - 1]) +
                         lambda1[time_steps + 1 - t] *  b1 * x[t - 1]));
    }
}

vec VolterraLotka::ComputeGradient()
{
    vec gradient(parameters_count, 0.0);

    for (u32 t = 0; t < time_steps; ++t)
    {
        gradient[0] -= lambda1[t] * x[t]        * tau;
        gradient[1] += lambda1[t] * x[t] * y[t] * tau;
        gradient[2] += lambda2[t]        * y[t] * tau;
        gradient[3] -= lambda2[t] * x[t] * y[t] * tau;
    }

    // for (u32 t = 0; t < time_steps - 1; ++t)
    // {
    //     gradient[0] -= (lambda1[t] * x[t] + lambda1[t + 1] * x[t + 1]) * tau * 0.5;
    //     gradient[1] += (lambda1[t] * x[t] * y[t] + lambda1[t + 1] * x[t + 1] * y[t + 1]) * tau * 0.5;
    //     gradient[2] += (lambda2[t] * y[t] + lambda2[t + 1] * y[t + 1]) * tau * 0.5;
    //     gradient[3] -= (lambda2[t] * x[t] * y[t] + lambda2[t + 1] * x[t + 1] * y[t + 1]) * tau * 0.5;
    // }

    return gradient;
}

vec VolterraLotka::ComputeGradientFDM(const double epsilon)
{
    vec gradient(parameters_count, 0.0);

    SolveForward(x, y, learning_parameters);

    current_loss = GetL2Loss();

    vec perturbated_parameters = learning_parameters;

    for (u32 i = 0; i < parameters_count; ++i)
    {
        perturbated_parameters[i] += epsilon;
        if (i > 0) perturbated_parameters[i - 1] -= epsilon;

        SolveForward(x, y, perturbated_parameters);

        const double perturbated_loss = GetL2Loss();
        
        gradient[i] = (perturbated_loss - current_loss) / epsilon;
    }

    return gradient;
}

double VolterraLotka::GetL2Loss() const
{
    double loss = 0;

    for (u32 t = 0; t < time_steps; ++t)
    {
        const double x_loss = (x[t] - x_data[t]) * (x[t] - x_data[t]);
        const double y_loss = (y[t] - y_data[t]) * (y[t] - y_data[t]);

        loss += x_loss + y_loss;
    }

    return loss * tau;
}

void VolterraLotka::ComputeGradientTheta(const double learning_rate,
                                         const double gamma)
{
    for (u32 t = 0; t < time_steps; ++t)
    {
        theta1[t] -= learning_rate * tau * (gamma * theta1[t] - lambda1[t]);
        theta2[t] -= learning_rate * tau * (gamma * theta2[t] - lambda2[t]);
    }
}                                         

void VolterraLotka::WriteData(const char* file_name, const vec& data) const
{
    std::ofstream file;
    file.open(file_name, std::ios::out);

    for (u32 i = 0; i < data.size(); ++i)
    {
        file << std::fixed << i * tau << "\t" << data[i] << std::endl;
    }

    file.close();
}
