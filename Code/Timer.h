#pragma once 

#include <chrono> 
#include <iostream>

struct Timer 
{
    std::chrono::time_point<std::chrono::steady_clock> start, end;

    Timer()
    {
        start = std::chrono::steady_clock::now();
    }

    ~Timer()
    {
        end = std::chrono::steady_clock::now();

        std::cout 
        << "Time difference = "
        << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
        << "[ms]" << std::endl;
    }
};