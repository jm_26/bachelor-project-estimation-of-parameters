#pragma once 

#include "BaseOptimizerODE.h"

#include "MyTypedefs.h"

class Lorenz : public BaseOptimizerODE
{

public: 

    Lorenz(const vec& known_parameters,
           const vec& learning_parameters,
           const double final_time,
           const double tau, 
           const vec& forward_initial_condition);

    void Optimize(GradientMethod method);

private: 

    const u32 parameters_count = 3;

    vec x, y, z; 

    vec x_data, y_data, z_data;

    vec lambda1, lambda2, lambda3;

    void SetInitialConditions();

    void SolveForward(vec& v1, vec& v2, vec& v3, const vec& params);

    void SolveAdjoint();

    vec ComputeGradient();

    vec ComputeGradientFDM(const double epsilon);

    double GetL2Loss() const;

    void WriteData(const char* file_name, const vec& data) const;
};
