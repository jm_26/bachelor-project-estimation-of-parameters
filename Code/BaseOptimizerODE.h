#pragma once 

#include "MyTypedefs.h"

enum class GradientMethod
{
    ADJOINT, 
    FDM
};

class BaseOptimizerODE
{

public: 

    BaseOptimizerODE(const vec& known_parameters,
                     const vec& learning_parameters,
                     const double final_time,
                     const double tau,
                     const vec& forward_initial_condition)
    : known_parameters(known_parameters),
      learning_parameters(learning_parameters),
      final_time(final_time),
      tau(tau),
      time_steps((u32)(final_time / tau)),
      forward_initial_condition(forward_initial_condition),
      current_loss(1e10)
    {}                  

protected: 

    const vec known_parameters;

    vec learning_parameters;

    const double final_time; 

    const double tau; 

    const u32 time_steps;

    const vec forward_initial_condition;

    double current_loss;
};