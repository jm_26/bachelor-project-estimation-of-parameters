# Estimation of Parameters of Ordinary and Partial Differential Equations

This repository contains C++ implementations for the numerical solutions to several models and the estimation of their parameters. Included are implementations for the Volterra-Lotka, SIR, Lorenz, and Gray-Scott models.

## Code Directory Structure

Each model is organized into its own pair of .h and .cpp files, with an additional .cpp file containing the main function.

For example:

### Volterra-Lotka Model
  - `VolterraLotka.h`: Header file containing the declarations for the Volterra-Lotka model.
  - `VolterraLotka.cpp`: Implementation file for the Volterra-Lotka model.
  - `volterra_lotka.cpp`: Main function demonstrating the usage of the Volterra-Lotka model.

Similar files are provided for the SIR, Lorenz, and Gray-Scott models.

## Additional Files

In addition to the model files, this repository contains several more files, including the `Algorithms` files and other supportive header files.

## Cloning the Repository

To clone this repository to your local machine, use the following command:

```bash
git clone https://gitlab.com/jm_26/bachelor-project-estimation-of-parameters.git
```

## Compilation and Execution

You may compile and execute each model using a C++ compiler that supports the C++ standard used in the code (e.g., C++11 or later). 
This project has no external dependencies.
Here's an example command for compiling the Volterra-Lotka model:

```bash
g++ -O3 volterra_lotka.cpp VolterraLotka.cpp Algorithms.cpp && ./a.out
```

The compilation of other models follows a similar pattern.
